def Tokenize(input:str):
    result = []
    validatetokens = ['push', 'pop', 'add', 'sub', 'mul', 'div', 'mod', 'skip', 'save', 'get']
    parts = input.split()
    for part in parts:
        if part in validatetokens:
            result.append(part)
        elif part.isnumeric() or part.startswith('-') and part[1:].isnumeric():
            result.append(part)
        else:
            expmsg = 'Unexpected Token: {}'.format(part)
            raise ValueError(expmsg)
    return result

def Parse(tokens:list):
    singletokens = ['pop', 'add', 'sub', 'mul', 'div', 'mod', 'skip']
    doubletokens = ['push', 'save', 'get']
    if len(tokens)==1 and tokens[0] in singletokens:
        pass
    elif len(tokens)==2 and tokens[0] in doubletokens and (tokens[1].isnumeric() or tokens[1].startswith('-') and tokens[1][1:].isnumeric()):
        tokens[1] = int(tokens[1])
        pass
    else:
        expmsg = 'Incorrect usage: {}'.format(' '.join(tokens))
        raise ValueError(expmsg)
