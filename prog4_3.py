import sys

from prog4_1 import *
from prog4_2 import *

def main():
    if len(sys.argv)==2:
        filepath = sys.argv[1]
        with open(filepath) as fr:
            lines = fr.readlines()
            tokenslist = []
            for line in lines:
                tokens = Tokenize(line)
                tokenslist.append(tokens)
            for tokens in tokenslist:
                Parse(tokens)
            stackMachine = StackMachine()
            print('Assignment #4-3, Haibo Zhou, 1050029976@qq.com')
            while stackMachine.CurrentLine<len(tokenslist):
                linenum = stackMachine.CurrentLine
                tokens = tokenslist[linenum]
                try:
                    r = stackMachine.Execute(tokens)
                    if r is not None:
                        print(r)
                except Exception as e:
                    print('Line {:d}: {:}'.format(linenum,e))

                if stackMachine.CurrentLine<0:
                    print('Trying to execute invalid line: {}'.format(linenum))
                    return
        print('Program terminated correctly')



if __name__=='__main__':
    main()