class StackMachine:

    def __init__(self) -> None:
        super().__init__()
        self.CurrentLine = 0
        self.stack = []
        self.memory = [0]*100

    def Execute(self, tokens:list):
        if len(tokens)==2 and tokens[0]=='push':
            self.stack.append(tokens[1])
            self.CurrentLine += 1
            return
        elif len(tokens)==1 and tokens[0]=='pop':
            self.CurrentLine += 1
            if len(self.stack)==0:
                raise IndexError('Invalid Memory Access')
            v = self.stack[-1]
            self.stack = self.stack[:-1]
            return v
        elif len(tokens)==1 and tokens[0]=='add':
            self.CurrentLine += 1
            if len(self.stack)<2:
                raise IndexError('Invalid Memory Access')
            v1 = self.stack[-1]
            v2 = self.stack[-2]
            self.stack = self.stack[:-2]
            self.stack.append(v1+v2)
            return
        elif len(tokens)==1 and tokens[0]=='sub':
            self.CurrentLine += 1
            if len(self.stack)<2:
                raise IndexError('Invalid Memory Access')
            v1 = self.stack[-1]
            v2 = self.stack[-2]
            self.stack = self.stack[:-2]
            self.stack.append(v1-v2)
            return
        elif len(tokens)==1 and tokens[0]=='mul':
            self.CurrentLine += 1
            if len(self.stack)<2:
                raise IndexError('Invalid Memory Access')
            v1 = self.stack[-1]
            v2 = self.stack[-2]
            self.stack = self.stack[:-2]
            self.stack.append(v1*v2)
            return
        elif len(tokens)==1 and tokens[0]=='div':
            self.CurrentLine += 1
            if len(self.stack)<2:
                raise IndexError('Invalid Memory Access')
            v1 = self.stack[-1]
            v2 = self.stack[-2]
            self.stack = self.stack[:-2]
            self.stack.append(v1/v2)
            return
        elif len(tokens)==1 and tokens[0]=='mod':
            self.CurrentLine += 1
            if len(self.stack)<2:
                raise IndexError('Invalid Memory Access')
            v1 = self.stack[-1]
            v2 = self.stack[-2]
            self.stack = self.stack[:-2]
            self.stack.append(v1%v2)
            return
        elif len(tokens)==1 and tokens[0]=='skip':
            self.CurrentLine += 1
            if len(self.stack)<2:
                raise IndexError('Invalid Memory Access')
            v1 = self.stack[-1]
            v2 = self.stack[-2]
            self.stack = self.stack[:-2]
            if v1==0:
                self.CurrentLine += v2
            return
        elif len(tokens)==2 and tokens[0]=='save':
            self.CurrentLine += 1
            if len(self.stack)==0:
                raise IndexError('Invalid Memory Access')
            v1 = self.stack[-1]
            self.stack = self.stack[:-1]
            self.memory[tokens[1]] = v1
            return
        elif len(tokens)==2 and tokens[0]=='get':
            v = self.memory[tokens[1]]
            self.stack.append(v)
            self.CurrentLine += 1
            return




        pass
